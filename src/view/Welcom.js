import moment from "moment";
import React, { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { mockComponent } from "react-dom/test-utils";
import { useHistory } from "react-router-dom";
import dataService from "../network/service";
const queryString = require("query-string");

function Welcom(props) {
  const [info, setInfo] = useState({});

  const history = useHistory();

  React.useEffect(() => {
    var isMobile = {
      Android: function () {
        return navigator.userAgent.match(/Android/i);
      },
      BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
      },
      iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
      },
      Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
      },
      Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
      },
      any: function () {
        return (
          isMobile.Android() ||
          isMobile.BlackBerry() ||
          isMobile.iOS() ||
          isMobile.Opera() ||
          isMobile.Windows()
        );
      },
    };
    const parsed = queryString.parse(window.location.search);
    if (parsed) {
      getInfo(parsed.token);
      if (isMobile.Android() || isMobile.iOS()) {
        window.open("hyundaime://khaosat" + parsed.token, "_self");
      }
      localStorage.setItem("KEY_VERIFY", parsed.token);
    }
  }, []);

  const getInfo = async (token) => {
    try {
      let rs = await dataService.getInfo({ surveyKey: token });
      if (!rs) return;
      if (rs.isDone) {
        return history.push("/completed");
      }
      if (rs.code == 99) {
        return history.push("/expired");
      }
      if (rs.code) {
        return alert(rs.message);
      }
      setInfo(rs.data.pushInfo);
    } catch (error) {}
  };

  console.log(info);
  return (
    <>
      <app-root _nghost-imn-c0="true" ng-version="7.2.15">
        <div
          id="header"
          style={{
            background: "transparent",
            margin: "0px auto",
            textAlign: "-webkit-center",
          }}
        >
          <img
            alt="Huyndai"
            className="d-xl-block"
            src="/images/logosplash.png"
            style={{ width: 287 }}
          />
        </div>
        <router-outlet />
        {Object.keys(info)?.length ? (
          <app-survey-sale>
            <div id="cBody">
              <div className="survey">
                <div className="ng-untouched ng-valid ng-dirty">
                  <div className="cus-info">
                    <div className="form-group">
                      <div
                        style={{
                          lineHeight: 1.5,
                          marginTop: 30,
                        }}
                      >
                        <b>HYUNDAI | TCMOTOR VIỆT NAM</b> <br />
                        Tổng đài:{" "}
                        <a href="tel:1900 561212">
                          <b>1900 561212</b>
                        </a>
                        <br />
                        Email:{" "}
                        <a href="mailto:cs@hyundai.thanhcong.vn">
                          <b>cs@hyundai.thanhcong.vn</b>
                        </a>
                        <br />
                        Web: <b>https://hyundai.thanhcong.vn</b>
                        <br />
                        Khảo Sát Sự Hài Lòng Của Khách Hàng HYUNDAI|TC MOTOR
                        <br />
                        Tại HYUNDAI|TC MOTOR, chúng tôi mong muốn mọi khách hàng
                        đều được trải nghiệm dịch vụ tốt nhất.
                        <br />
                        Đó là lý do tại sao chúng tôi muốn tham khảo ý kiến trải
                        nghiệm của quý khách tại Đại lý Hyundai ủy quyền.
                        <br />
                        Mọi ý kiến của quý khách đều được bảo mật.
                        <br />
                        Chúng tôi mong nhận được khảo sát đã hoàn tất từ quý
                        khách. Xin cảm ơn.
                        <br />
                        Có hiệu lực đến:{" "}
                        {moment(info?.expiredTime).format("DD/MM/YYYY")}
                        <br />
                      </div>
                    </div>
                  </div>
                  <div
                    className="btnArea"
                    style={{ display: "block", marginBottom: 20 }}
                  >
                    <button
                      onClick={() => history.push("/info")}
                      className="button blueBt nextBt"
                      type="submit"
                      style={{ width: "50%" }}
                    >
                      Bắt đầu
                    </button>
                  </div>

                  <div style={{ height: 70 }} />
                </div>
              </div>
            </div>
          </app-survey-sale>
        ) : (
          <div
            style={{
              top: 80,
              position: "absolute",
              left: 0,
              right: 0,
              display: "flex",
              justifyContent: "center",
            }}
          >
            <b>Khảo sát không tồn tại</b> <br />
          </div>
        )}
        <div id="footer">
          {" "}
          Copyright © 2018 Hyundai Motor Company. All rights reserved.
        </div>
      </app-root>
    </>
  );
}

export default Welcom;
