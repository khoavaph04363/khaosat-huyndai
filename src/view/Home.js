/* eslint-disable no-undef */
import moment from "moment";
import React, { useEffect, useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { mockComponent } from "react-dom/test-utils";
import { useHistory } from "react-router-dom";
import dataService from "../network/service";
import SelectSearch from "react-select-search";
const queryString = require("query-string");

const checkPhoneNumber = (phone) => {
  let checkPhone = /(0[3|5|7|8|9])([0-9]{8})\b/;
  let regex = phone.match(checkPhone); //  các đầu số của các nhà mạng di dộng của Việt Nam
  return regex;
};
const validateEmail = (email) => {
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
};

const removeAgencyNS = (agency, type) => {
  if (!agency) {
    return [];
  }
  const agencyRs = [];
  if (type == "services") {
    for (let index = 0; index < agency.length; index++) {
      if (
        (agency[index].allowAutoSurveySSI =
          true && agency[index].indicatorsSSI > 0)
      ) {
        agencyRs.push(agency[index]);
      }
    }
  } else {
    for (let index = 0; index < agency.length; index++) {
      if (
        (agency[index].allowAutoSurveyCSI =
          true && agency[index].indicatorsCSI > 0)
      ) {
        agencyRs.push(agency[index]);
      }
    }
  }
  return agencyRs;
};

function Home(props) {
  const [endDate, setEndDate] = useState(new Date());
  const [info, setInfo] = useState({});
  const [cars, setCars] = useState([]);
  const [agencys, setAgency] = useState([]);

  const [userInfo, setUserInfo] = useState({});
  const [typeService, setTypeService] = useState("");
  const [showOtherAddress, setShowOtherAddress] = useState(false);

  const history = useHistory();

  let {
    otherAddress,
    agencyAddress,
    agencyName,
    carName,
    customerName,
    customerPhone,
    customerAddress,
    customerEmail,
    plateNo,
    carId,
  } = info || {};

  let isMobile = {
    Android: function () {
      return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
      return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
      return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
      return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
      return (
        isMobile.Android() ||
        isMobile.BlackBerry() ||
        isMobile.iOS() ||
        isMobile.Opera() ||
        isMobile.Windows()
      );
    },
  };

  useEffect(() => {
    if (agencyAddress == "khac") {
      setShowOtherAddress(true);
    } else {
      setShowOtherAddress(false);
      onChangevalue({
        target: {
          name: "otherAddress",
          value: "",
        },
      });
    }
  }, [agencyAddress]);

  const agencyOptions = agencys?.map((item, index) => ({
    name: item.name,
    value: item.name,
    id: item.id,
    code: item.code,
  }));

  const onChangevalue = (val) => {
    let tmp = { ...info };
    tmp[val.target.name] = val.target.value;
    setInfo(tmp);
  };

  const getCars = async () => {
    try {
      let rs = await dataService.getCars({ skip: 0, limit: 1000000 });
      if (!rs || rs.code) return;
      setCars(rs.data);
    } catch (error) {
      console.log(error);
    }
  };
  const getAgency = async (type) => {
    try {
      let rs = await dataService.getAgency({ skip: 0, limit: 1000000 });
      if (!rs || rs.code) return;
      let tmp = removeAgencyNS(rs.data, type);
      setAgency(tmp);
    } catch (error) {
      console.log(error);
    }
  };

  const getInfo = async (token) => {
    try {
      let rs = await dataService.getInfo({ surveyKey: token });
      if (!rs) return;
      if (rs.isDone) {
        return history.push("/completed");
      }
      setUserInfo(rs.data.pushInfo);
      setInfo({
        ...rs.data.pushInfo,
        listAddress: rs.data.pushInfo?.agencyAddress,
        // agencyName: "",
      });
      if (rs.data?.pushInfo?.type == "services") {
        setEndDate(rs?.data.pushInfo?.completeServiceTime);
      } else {
        setEndDate(rs?.data.pushInfo?.completeServiceTime);
      }
      getAgency(rs.data?.pushInfo?.type);
    } catch (error) {
      console.log(error);
    }
  };

  const onSubmit = async () => {
    try {
      let includeCard = cars.some((i) => i.name == carName);

      // let { agencyName, carName, customerName, customerPhone, customerAddress, customerEmail } = info
      if (
        (agencyAddress == "khac" && !otherAddress) ||
        !agencyName ||
        !carName ||
        carName === true ||
        carName == "Chọn tên xe" ||
        !includeCard ||
        !customerName ||
        !customerPhone ||
        !customerAddress ||
        !endDate
      )
        return alert("Yêu cầu nhập những trường bắt buộc");
      if (info.type == "services" && !typeService)
        return alert("Yêu cầu nhập dịch vụ KH đã sử dụng");
      if (info.type == "services" && !plateNo)
        return alert("Yêu cầu nhập biển kiểm soát");
      if (!checkPhoneNumber(customerPhone))
        return alert("Yêu cầu nhập số điện thoại đúng định dạng");
      if (!validateEmail(customerEmail) && customerEmail)
        return alert("Yêu cầu nhập email đúng định dạng");
      let rs = await dataService.updateInfo({
        ...info,
        completeForm: isMobile.Android() || isMobile.iOS() ? "app" : "web",
        typeName: typeService,
        completeServiceTime: moment(endDate).valueOf(),
        key: localStorage.getItem("KEY_VERIFY"),
        agencyAddress:
          agencyAddress == "khac"
            ? otherAddress
            : typeof agencyAddress == "object"
            ? agencyAddress[0]
            : agencyAddress,
      });
      if (rs.code) return alert(rs.message);
      history.push("/form");
    } catch (error) {
      console.log(error);
    }
  };

  React.useEffect(() => {
    const parsed = queryString.parse(window.location.search);
    if (parsed) {
      // if (isMobile.Android() || isMobile.iOS()) {
      //   window.open('hyundaime://khaosat' + parsed.token, '_self')
      // }
      // if (parsed.token) {
      //   localStorage.setItem('KEY_VERIFY', parsed.token)
      // }

      getInfo(localStorage.getItem("KEY_VERIFY"));
    }
    getCars();
  }, []);

  useEffect(() => {
    if (agencys?.length) {
      const activeAgency = agencys?.find((i) => i.name == agencyName);
      let ltsAddress = agencys?.filter(
        (i) => i?.agencyCode == activeAgency?.agencyCode
      );

      let tmp = ltsAddress?.map((i) => i.workshopAddress);
      setInfo({
        ...info,
        listAddress: tmp,
        agencyAddress: tmp[0],
      });

      console.log(tmp);
    }
  }, [agencyName, agencys]);

  return (
    <>
      <app-root _nghost-imn-c0="true" ng-version="7.2.15">
        <div
          id="header"
          style={{
            background: "transparent",
            margin: "0px auto",
            textAlign: "-webkit-center",
          }}
        >
          <img
            alt="Huyndai"
            className="d-none d-xl-block"
            src="/images/logosplash.png"
            style={{ width: 287 }}
          />
        </div>
        <router-outlet />
        <app-survey-sale>
          <div />
          <div id="cBody">
            <div className="survey">
              <div className="ng-untouched ng-valid ng-dirty">
                <div className="cus-info">
                  <div className="form-group">
                    <label className="text-red">* Bắt buộc phải nhập</label>
                  </div>
                  <div className="form-group">
                    <label className="required">Họ và tên:</label>
                    <div>
                      <input
                        value={customerName}
                        className="form-control ng-untouched ng-pristine ng-valid"
                        name="customerName"
                        required
                        type="text"
                        onChange={onChangevalue}
                      />
                    </div>
                    {/**/}
                  </div>
                  <div className="form-group">
                    <label className="required">Số điện thoại:</label>
                    <div>
                      <input
                        value={customerPhone}
                        className="form-control ng-untouched ng-pristine ng-valid"
                        name="customerPhone"
                        required
                        type="text"
                        onChange={onChangevalue}
                      />
                    </div>
                    {/**/}
                  </div>
                  <div className="form-group">
                    <label>Địa chỉ email:</label>
                    <div>
                      <input
                        value={customerEmail}
                        className="form-control ng-untouched ng-pristine ng-valid"
                        email
                        name="customerEmail"
                        type="email"
                        onChange={onChangevalue}
                      />
                    </div>
                    {/**/}
                  </div>
                  <div className="form-group">
                    <label className="required">Địa chỉ hiện tại:</label>
                    <div>
                      <input
                        value={customerAddress}
                        className="form-control ng-untouched ng-pristine ng-valid"
                        name="customerAddress"
                        required
                        type="text"
                        onChange={onChangevalue}
                      />
                    </div>
                  </div>

                  <div className="form-group">
                    <label
                      className={info?.type == "services" ? "required" : ""}
                    >
                      Biển kiểm soát :
                    </label>
                    <div>
                      <input
                        value={plateNo}
                        className="form-control ng-untouched ng-pristine ng-valid"
                        name="plateNo"
                        required
                        type="text"
                        onChange={onChangevalue}
                      />
                    </div>
                  </div>
                  {/**/}
                  <div className="form-group">
                    {/**/}
                    {/**/}
                    <label className="required">
                      Loại xe Hyundai Quý khách đã mua:{" "}
                    </label>
                    <div>
                      <select
                        onChange={onChangevalue}
                        className="form-control select ng-untouched ng-pristine ng-valid"
                        name="carName"
                        required
                        value={carName}
                      >
                        <option key={-1} value={undefined}>
                          Chọn tên xe
                        </option>

                        {cars.map((item, index) => (
                          <option key={index} value={item.name}>
                            {item.name}
                          </option>
                        ))}
                      </select>
                    </div>
                    {/**/}
                  </div>
                  {info.type == "services" ? (
                    <div className="form-group">
                      <label>Thời điểm làm dịch vụ gần nhất:</label>
                      <div>
                        <DatePicker
                          disabled
                          locale="vi"
                          selected={endDate}
                          dateFormat="dd/MM/yyyy"
                          onChange={(date) => {
                            setEndDate(date);
                          }}
                        />
                      </div>
                    </div>
                  ) : (
                    <div className="form-group">
                      <label>Thời điểm nhận xe:</label>
                      <div>
                        <DatePicker
                          disabled
                          locale="vi"
                          selected={endDate}
                          dateFormat="dd/MM/yyyy"
                          onChange={(date) => {
                            setEndDate(date);
                          }}
                        />
                      </div>
                    </div>
                  )}
                  {/* <div className="form-group">
                   
                    <label className="required">
                      {info.type == "services"
                        ? "Tên Đại lý Quý khách đã thực hiện dịch vụ"
                        : "Tên đại lý khách hàng đã mua xe"}
                    </label>
                
                    <div>
                      <input
                        // disabled
                        onChange={onChangevalue}
                        className="form-control ng-untouched ng-pristine"
                        value={agencyName}
                        name="agencyName"
                        required
                        type="text"
                      />
                    </div>
                   
                  </div> */}

                  <div className="form-group">
                    <label>
                      {info.type == "services"
                        ? "Tên Đại lý Quý khách đã thực hiện dịch vụ"
                        : "Tên đại lý khách hàng đã mua xe"}
                    </label>
                    <div>
                      {/* <select
                        onChange={onChangevalue}
                        className="form-control select ng-untouched ng-pristine ng-valid"
                        name="agencyName"
                        required
                        value={agencyName}
                       
                      >
                        <option value>Chọn tên đại lý</option>

                        {agencys?.map((item, index) => (
                          <option key={index} value={item.name}>
                            {item.name}
                          </option>
                        ))}
                      </select> */}

                      <SelectSearch
                        search="search"
                        autoComplete="on"
                        onChange={(val) =>
                          onChangevalue({
                            target: { name: "agencyName", value: val },
                          })
                        }
                        // className="form-control select ng-untouched ng-pristine ng-valid"
                        options={agencyOptions}
                        value={agencyName}
                        name="agencyName"
                        placeholder="Chọn đại lý "
                      />
                    </div>
                  </div>

                  <div className="form-group">
                    <label>
                      {/* Đại lý ở Tỉnh/Thành phố nào? */}
                      Địa chỉ đại lý
                    </label>
                    <div>
                      <select
                        onChange={onChangevalue}
                        className="form-control select ng-untouched ng-pristine ng-valid"
                        name="agencyAddress"
                        required
                        value={agencyAddress}
                      >
                        {/* <option value>
                          Chọn đại chỉ đại lý
                        </option> */}

                        {info?.listAddress?.map((item, index) => (
                          <option key={index} value={item}>
                            {item}
                          </option>
                        ))}

                        <option key={"khac"} value={"khac"}>
                          Khác
                        </option>
                      </select>
                    </div>
                    <div style={{ height: 10 }} />
                    {showOtherAddress ? (
                      <input
                        value={otherAddress}
                        className="form-control ng-untouched ng-pristine ng-valid"
                        name="otherAddress"
                        required
                        type="text"
                        placeholder="Nhập địa chỉ khác"
                        onChange={onChangevalue}
                      />
                    ) : null}
                  </div>

                  {info.type == "services" ? (
                    <div className="form-group">
                      <label className="required">
                        Loại hình dịch vụ KH đã sử dụng
                      </label>
                      <div>
                        <select
                          onChange={(val) => setTypeService(val.target.value)}
                          className="form-control select ng-untouched ng-pristine ng-valid"
                          name="typeName"
                          required
                          value={typeService}
                        >
                          <option value={false}>Chọn loại hình dịch vụ</option>

                          {["Bảo hành", "Sửa chữa", "Bảo dưỡng"].map(
                            (item, index) => (
                              <option key={index} value={item}>
                                {item}
                              </option>
                            )
                          )}
                        </select>
                      </div>
                    </div>
                  ) : null}
                  {/**/}
                  {/**/}
                </div>
                <div
                  className="btnArea"
                  style={{ display: "block", marginBottom: 20 }}
                >
                  {/* <a className="grayBt prevBt" href="/">
                    Quay lại
                  </a> */}
                  <button
                    onClick={onSubmit}
                    className="button blueBt nextBt"
                    type="submit"
                    style={{ width: "50%" }}
                  >
                    Tiếp tục
                  </button>
                </div>

                <div style={{ height: 70 }} />
              </div>
            </div>
          </div>
        </app-survey-sale>
        <div id="footer">
          {" "}
          Copyright © 2018 Hyundai Motor Company. All rights reserved.
        </div>
      </app-root>
    </>
  );
}

export default Home;
