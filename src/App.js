import React from "react";
import "react-select-search/style.css";
// import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./view/Home";
import Form from "./view/Form";
import Completed from "./view/Completed";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import "./network/axiosConfig";
import moment from "moment";
import Welcom from "./view/Welcom";
import Expired from "./view/Expired";

const routes = [
  {
    path: "/",
    exact: true,
    Component: () => <Welcom />,
  },
  {
    path: "/info",
    exact: true,
    Component: () => <Home />,
  },
  {
    path: "/form",
    exact: true,
    Component: () => <Form />,
  },
  {
    path: "/completed",
    exact: true,
    Component: () => <Completed />,
  },
  {
    path: "/expired",
    exact: true,
    Component: () => <Expired />,
  },
];

function showRoute(routes) {
  var result = null;
  if (routes.length > 0) {
    result = routes.map((route, index) => {
      return (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={(props) => {
            // api.setHistory(props.history)
            let Screen = route.Component;
            return <Screen />;
          }}
        />
      );
    });
  }
  return <Switch>{result}</Switch>;
}

function App() {
  return (
    <div className="App">
      <TransitionGroup>
        <CSSTransition
          key={moment().valueOf()}
          timeout={500}
          //    style='w
        >
          <BrowserRouter>{showRoute(routes)}</BrowserRouter>
        </CSSTransition>
      </TransitionGroup>
    </div>
  );
}

export default App;
