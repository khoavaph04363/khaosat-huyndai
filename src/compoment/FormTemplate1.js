import React, { useState, useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

function FormTemplate1(props) {
  const [idCheck, setIdCheck] = useState('')
  const { onNextIndex, planInfo, planDetails, index } = props
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);

  const activeQuestion = planDetails[index]

  const clickOption = (id) => {
    if (activeQuestion?.allowMultipleOptionAnswer) {
      if (idCheck.includes(id)) {
        setIdCheck((val) => {
          return val.replace('' + id, "")
        })
      } else {
        setIdCheck((val) => {
          if (!val.length) return id + ''
          return val + ',' + id
        })
      }
      return
    }
    setIdCheck(id)
  }

  return (
    <>
      <app-root ng-version="7.2.15">
        <div
          id="header"
          style={{
            background: "transparent",
            margin: "0 auto",
            textAlign: "-webkit-center",
          }}
        >
          <img
            alt="Huyndai"
            className="d-none d-xl-block"
            src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
            style={{ width: 287 }}
          />
        </div>
        <router-outlet />
        <app-survey-sale>
          <div />
          <div id="cBody">
            <div className="survey">
              <div
                className="imgArea animate__animated animate__fadeInDownBig"
                data-aos="fade-down"
                data-aos-duration="600"
                data-aos-offset="200"
              >
                <div className="bg">
                  <div className="txtArea">
                    <p className="num">{index + 1} / {planDetails?.length}</p>
                    <p className="tit">{planInfo?.name}</p>
                  </div>
                </div>
                {/* <img
                    alt="Huyndai"
                    className="d-none d-xl-block"
                    src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
                    style={{ width: 287 }}
                /> */}
                <img
                  alt="Huyndai"
                  src={activeQuestion?.images[0] || process.env.PUBLIC_URL + "/images/hhh.gif"}
                  style={{
                    display: "inline",
                    objecFit: "cover",
                    width: "100%",
                    height: "100%",
                  }}
                />
              </div>
              <div
                className="surveyArea"
                data-aos="fade-up"
                data-aos-duration="600"
                data-aos-offset="200"
              >
                <div className="ng-untouched ng-pristine ng-valid">
                  <div className="qArea on animate__animated animate__fadeInUpBig">
                    <div className="conDiv">
                      <div className="table">
                        <div className="table-cell 1">

                          <div className="txtArea" style={{ display: "block" }}>
                            <div className="table on">
                              <div className="table-cell">
                                <p className="q">
                                  <img
                                    src={
                                      process.env.PUBLIC_URL +
                                      "/images/q_tit2.png"
                                    }
                                    alt="Q+"
                                  />
                                </p>
                                <p className="qTxt">
                                  {activeQuestion?.name}
                                </p>
                                {/**/}
                                <div className="checkArea">
                                  {/**/}
                                  {activeQuestion?.questionOptions.map((item, idx) => <p key={idx} className="check">
                                    <input
                                      onClick={() => clickOption(item.id)}
                                      className="check-answer"
                                      name="check_answer"
                                      type="checkbox"
                                      id={"chk1_" + idx}
                                      checked={idCheck.includes(item.id)}

                                    />
                                    <label htmlFor={"chk1_" + idx}>
                                      {item.name}
                                    </label>
                                  </p>)}



                                  <div style={{ height: 50 }} />

                                  {/**/}
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="btnArea" style={{ display: "block" }}>
                            <a className="grayBt prevBt" href="javascript:">
                              Quay lại
                            </a>
                            <a onClick={() => onNextIndex(idCheck)} href="javascript:" className="blueBt nextBt ">
                              Tiếp Tục
                            </a>
                            {/**/}
                          </div>
                        </div>
                        {/**/}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </app-survey-sale>
        <div id="footer">
          {" "}
          Copyright © 2018 Hyundai Motor Company. All rights reserved.
        </div>
      </app-root>
    </>
  );
}

export default FormTemplate1;
