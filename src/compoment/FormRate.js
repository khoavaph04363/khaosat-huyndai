import React, { useEffect, useState } from 'react'
import dataService from '../network/service'
import { emitPubsub } from '../pubsub'
import FormCheckBox from './FormCheckBox'
import FormInput from './FromInput'
const FromRate = (props) => {
    const [maxRange, setMaxRange] = useState()
    const { activeQuestion } = props
    const { setSubPlain, isSub, clickOptionCheckBox, idCheck } = props
    useEffect(() => {
        console.log(maxRange)
        if (maxRange || maxRange == 0) {
            let currentRange = maxRange + 1
            clickOptionCheckBox(currentRange)
        }
    }, [maxRange])

    useEffect(() => {
        if (!idCheck) {
            setMaxRange(undefined)
        }
        console.log(idCheck, 'idcheck')


    }, [idCheck])

    const classRange = (index) => {
        let tmpClass = ""
        if (!index) {
            tmpClass = "fir"
        }
        if (index == 9) {
            tmpClass = "last"
        }
        if (index <= maxRange) {
            tmpClass = tmpClass + " on"
        }
        if (index == maxRange) {
            tmpClass = tmpClass + " selected"
        }
        return tmpClass
    }


    return (
        <>
            <div className="txtArea on">
                <div className="table">
                    <div className="table-cell">
                        <br />
                        <br />
                        <br />
                        <br />
                        {/* <p className="q">
                            <img
                                src={
                                    process.env.PUBLIC_URL +
                                    "/images/q_tit1.png"
                                }
                                alt="Q"
                            />
                        </p> */}
                        <p className="qTxt">
                            {/**/}
                            <span className="text-danger">{activeQuestion?.allowMultipleOptionAnswer ? '' : '*'}</span> {activeQuestion.name}
                        </p>
                        <p className="pcTxt">
                            {activeQuestion?.subtext || ''}
                        </p>
                        {/**/}
                        <div className="scoreArea">
                            <div className="btns">
                                <span className="bad">1</span>
                                {new Array(10).fill(1).map((i, idx) => {
                                    return <a key={idx} style={{ cursor: 'pointer' }} onClick={() => setMaxRange(idx)} data-point={idx + 1} className={classRange(idx)}>
                                        <span>{idx + 1}</span>
                                    </a>
                                })}
                                <span className="good">10</span>
                            </div>
                        </div>
                        <div className="mobileScore">
                            <select
                                value={idCheck}
                                onChange={(val) => {

                                    return setMaxRange(val.target.value - 1)
                                }}
                                name="rating"
                                className="ng-pristine ng-valid ng-touched"
                            >
                                <option key={0} value={undefined}>Select</option>

                                {new Array(10).fill(1).map((i, idx) => <option key={idx + 1} value={idx + 1}>{idx + 1} </option>)}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default FromRate
