import React, { useState, useEffect } from 'react'
const FormCheckBox = (props) => {
    const [tmpKey, setTmpKey] = useState('')
    const { activeQuestion, idCheck, clickOptionCheckBox, setSubPlain, isSub,
        otherCheckBox,
        setOtherCheckbox
    } = props


    const clickOptionTmp = (id) => {
        if (activeQuestion?.allowMultipleOptionAnswer) {
            if (tmpKey.includes(id)) {
                setTmpKey((val) => {
                    return val.replace('' + id, "")
                })
            } else {
                setTmpKey((val) => {
                    if (!val.length) return id + ''
                    return val + ',' + id
                })
            }
            return
        }
        setTmpKey(id)
    }

    useEffect(() => {
        if (!idCheck) {
            clickOptionTmp('')
        }
    }, [idCheck])
    return (
        <div className="txtArea" style={{ display: "block" }}>
            <div className="table on">
                <div className="table-cell">
                    {/* <p className="q">
                        <img
                            src={
                                process.env.PUBLIC_URL +
                                "/images/q_tit2.png"
                            }
                            alt="Q+"
                        />
                    </p> */}
                    <p className="qTxt">
                        <span className="text-danger">{activeQuestion?.allowMultipleOptionAnswer ? '' : '*'}</span>{activeQuestion?.name}
                    </p>
                    {/**/}
                    <div className="checkArea">
                        {/**/}
                        {activeQuestion?.questionOptions.map((item, idx) => <p key={idx + isSub + ''} className="check">
                            <input
                                onClick={() => {
                                    clickOptionTmp(item.key)
                                    clickOptionCheckBox(item.key)
                                }}
                                className="check-answer"
                                name="check_answer"
                                type="checkbox"
                                id={"chk1_" + idx + isSub}
                                checked={String(idCheck).includes(item.key)}
                            />
                            <label htmlFor={"chk1_" + idx + isSub}>
                                {item.value}
                            </label>
                        </p>)}

                        {tmpKey?.includes('#') ? <div >
                            <label>Câu trả lời "khác" của bạn</label>
                            <textarea
                                onChange={(val) => {
                                    setOtherCheckbox(val.target.value)
                                }}
                                value={otherCheckBox}
                                id
                                name="comment"
                                rows={5}
                                style={{ width: "90%" }}
                                className="ng-pristine ng-valid ng-touched"
                                defaultValue={""}
                            />
                        </div> : null}
                        <div style={{ height: 50 }} />
                        {/**/}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default FormCheckBox
