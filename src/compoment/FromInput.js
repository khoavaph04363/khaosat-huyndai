import React, { useState } from 'react'


const FormInput = (props) => {
    const { activeQuestion, setValueTypeText, idCheck } = props

    return (
        <div className="txtArea on">
            <div className="table">

                {/* <p className="q">
                    <img
                        src={
                            process.env.PUBLIC_URL +
                            "/images/q_tit1.png"
                        }
                        alt="Q"
                    />
                </p> */}
                <p className="qTxt">
                    <span className="text-danger">{activeQuestion?.allowMultipleOptionAnswer ? '' : '*'}</span> {activeQuestion?.name}
                </p>
                <p className="pcTxt" />
                <p className="mobileTxt" />
                <div >
                    <textarea
                        onChange={setValueTypeText}
                        value={idCheck}
                        id
                        name="comment"
                        rows={5}
                        style={{ width: "90%" }}
                        className="ng-pristine ng-valid ng-touched"
                        defaultValue={""}
                    />
                </div>
            </div>
        </div>
    )
}

export default FormInput

