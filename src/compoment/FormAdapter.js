import React, { useState, useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";
import { useHistory } from "react-router";
import FormCheckBox from "./FormCheckBox";
import FromRate from "./FormRate";
import FormInput from "./FromInput";
import { CSSTransition, TransitionGroup } from "react-transition-group";
function FormAdapter(props) {
    const [depenQuestion, setDepenQuestion] = useState({})
    const [idCheck, setIdCheck] = useState('')
    const [otherCheckBox, setOtherCheckbox] = useState('')

    const [ready, setReady] = useState(false)

    const { page, onNextIndex, planInfo, planDetails, index, total, activePlain, onPreIndex, setActivePlain, subPlain, setSubPlain } = props

    const history = useHistory()
    useEffect(() => {
        setTimeout(() => {
            setReady(true)
        }, 500)
    }, []);



    const clickOptionCheckBox = (id) => {
        if (activePlain?.allowMultipleOptionAnswer && activePlain?.inputType == "checkbox") {
            if (idCheck.includes(id)) {
                setIdCheck((val) => {
                    return val.replace('' + id, "")
                })
            } else {
                setIdCheck((val) => {
                    if (!val.length) return id + ''
                    return val + ',' + id
                })
            }
            return
        }
        setIdCheck(id)
    }

    const setValueTypeText = (val, type) => {
        setIdCheck(val.target.value)
    }

    const defaulPropsForm = {
        idCheck,
        setValueTypeText,
        activeQuestion: activePlain,
        onNextIndex,
        planInfo,
        planDetails,
        index,
        setDepenQuestion,
        clickOptionCheckBox,
        setActivePlain,
        subPlain,
        setSubPlain,
        otherCheckBox,
        setOtherCheckbox
    }

    const renderForm = () => {
        switch (activePlain?.inputType) {
            case 'text':
                return <FormInput {...defaulPropsForm} />
            case "weight-point":
                return <FromRate {...defaulPropsForm} />
            case "checkbox":
                return <FormCheckBox {...defaulPropsForm} />
            default:
                break;
        }
    }
    return (
        <TransitionGroup
        >
            <CSSTransition
                key={activePlain?.id}
                timeout={500}
            >
                <app-root
                    style={{ display: ready ? undefined : 'none' }}
                    ng-version="7.2.15">
                    <div
                        id="header"
                        style={{
                            background: "transparent",
                            margin: "0 auto",
                            textAlign: "-webkit-center",
                        }}
                    >
                        <img
                            alt="Huyndai"
                            className="d-none d-xl-block"
                            src={
                                process.env.PUBLIC_URL +
                                "/images/logosplash.png"
                            }
                            style={{ width: 287 }}
                        />
                    </div>
                    <router-outlet />
                    <app-survey-sale>
                        <div />
                        <div id="cBody">
                            <div className="survey">
                                <div
                                    className="imgArea animate__animated animate__fadeInDownBig"
                                    data-aos="fade-down"
                                    data-aos-duration="600"
                                    data-aos-offset="200"
                                >
                                    <div className="bg">
                                        <div className="txtArea">
                                            {/* <p className="num">{page} / {planDetails?.length}</p> */}
                                            <p className="tit">{activePlain?.title}</p>
                                        </div>
                                    </div>
                                    {/* <img
                    alt="Huyndai"
                    className="d-none d-xl-block"
                    src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
                    style={{ width: 287 }}
                /> */}
                                    <img
                                        alt="Huyndai"
                                        src={activePlain?.images ? activePlain?.images[0] : ''}
                                        style={{
                                            display: "inline",
                                            objecFit: "cover",
                                            width: "100%",
                                            height: "100%",
                                        }}
                                    />
                                </div>
                                <div
                                    className="surveyArea"
                                    data-aos="fade-up"
                                    data-aos-duration="600"
                                    data-aos-offset="200"
                                >
                                    <div className="ng-untouched ng-pristine ng-valid">
                                        <div className="qArea on animate__animated animate__fadeInUpBig">
                                            <div className="conDiv">
                                                <div className="table">
                                                    <div className="table-cell 1">
                                                        {renderForm()}
                                                        <div className="btnArea" style={{ display: "block" }}>
                                                            <a onClick={() => {

                                                                if (activePlain?.id == planDetails[0]?.id) return history.goBack()
                                                                onPreIndex()
                                                                setIdCheck('')
                                                            }} className="grayBt prevBt" href="javascript:">
                                                                Quay lại
                                                            </a>
                                                            <a onClick={() => {
                                                                if (String(idCheck).includes('#') && !otherCheckBox) {
                                                                    return alert('Vui lòng nhập câu trả lời của bạn')
                                                                }
                                                                if (otherCheckBox) {
                                                                    let tmp = idCheck.split(',')
                                                                    let idxRm = tmp.findIndex(i => String(i).includes('#'))
                                                                    if (idxRm >= 0) {
                                                                        tmp.splice(idxRm, 1)
                                                                    }
                                                                    let jtmp = tmp.join()
                                                                    if (activePlain.allowMultipleOptionAnswer) {
                                                                        onNextIndex(jtmp + ',' + `#.${otherCheckBox}`)
                                                                    } else {
                                                                        onNextIndex(`#.${otherCheckBox}`)
                                                                    }
                                                                } else {
                                                                    onNextIndex(idCheck)
                                                                }
                                                                setIdCheck('')
                                                                setOtherCheckbox('')
                                                            }} href="javascript:" className="blueBt nextBt ">
                                                                Tiếp Tục
                                                            </a>
                                                            {/**/}
                                                        </div>
                                                    </div>
                                                    {/**/}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </app-survey-sale>
                    <div id="footer">
                        {" "}
                        Copyright © 2018 Hyundai Motor Company. All rights reserved.
                    </div>

                </app-root>
            </CSSTransition>
        </TransitionGroup>

    );
}
export default FormAdapter;
