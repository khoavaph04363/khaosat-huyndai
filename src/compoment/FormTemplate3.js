import React, { useState, useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

function FormTemplate3(props) {
  const { onNextIndex, planInfo, planDetails, index } = props
  const [content, setContent] = useState('')
  const activeQuestion = planDetails[index]

  useEffect(() => {
    AOS.init();
    AOS.refresh();
    setContent('')
  }, [index]);




  return (
    <>
      <app-root ng-version="7.2.15">
        <div
          id="header"
          style={{
            background: "transparent",
            margin: "0 auto",
            textAlign: "-webkit-center",
          }}
        >
          <img
            alt="Huyndai"
            className="d-none d-xl-block"
            src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
            style={{ width: 287 }}
          />
        </div>
        <router-outlet />
        <app-survey-sale>
          <div />
          <div id="cBody">
            <div className="survey">
              <div
                className="imgArea animate__animated animate__fadeInDownBig"
                data-aos="fade-down"
                data-aos-duration="600"
                data-aos-offset="200"
              >
                <div className="bg">
                  <div className="txtArea">
                    <p className="num">{index + 1} / {planDetails?.length}</p>
                    <p className="tit">{planInfo?.name}</p>
                  </div>
                </div>
                {/* <img
                    alt="Huyndai"
                    className="d-none d-xl-block"
                    src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
                    style={{ width: 287 }}
                /> */}
                <img
                  alt="Huyndai"
                  src={process.env.PUBLIC_URL + "/images/hhh.gif"}
                  style={{
                    display: "inline",
                    objecFit: "cover",
                    width: "100%",
                    height: "100%",
                  }}
                />
              </div>
              <div
                className="surveyArea"
                data-aos="fade-up"
                data-aos-duration="600"
                data-aos-offset="200"
              >
                <form noValidate className="ng-untouched ng-pristine ng-valid">
                  <div className="qArea on animate__animated animate__fadeInUpBig">
                    <div className="conDiv">
                      <div className="table">
                        <div className="table-cell 2">
                          <div className="txtArea on">
                            <div className="table">
                              <br />
                              <br />
                              <br />
                              <br />
                              <p className="q">
                                <img
                                  src={
                                    process.env.PUBLIC_URL +
                                    "/images/q_tit1.png"
                                  }
                                  alt="Q"
                                />
                              </p>
                              <p className="qTxt">
                                <span className="text-danger">*</span> {activeQuestion?.name}
                              </p>
                              <p className="pcTxt" />
                              <p className="mobileTxt" />
                              <div >
                                <textarea
                                  onChange={(val) => setContent(val.target.value)}
                                  value={content}
                                  id
                                  name="comment"
                                  rows={5}
                                  style={{ width: "90%" }}
                                  className="ng-pristine ng-valid ng-touched"
                                  defaultValue={""}
                                />
                              </div>
                            </div>
                          </div>

                          <div className="btnArea" style={{ display: "block" }}>
                            <a className="grayBt prevBt" href="javascript:">
                              Quay lại
                            </a>
                            <a onClick={() => onNextIndex(content)} href="javascript:" className="blueBt nextBt ">
                              Tiếp Tục
                            </a>
                            {/**/}
                          </div>
                        </div>

                        {/**/}
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </app-survey-sale>
        <div id="footer">
          {" "}
          Copyright © 2018 Hyundai Motor Company. All rights reserved.
        </div>
      </app-root>
    </>
  );
}

export default FormTemplate3;
