import React, { useState, useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";
import { useHistory } from "react-router";

function TemplateSelect(props) {
    const [idCheck, setIdCheck] = useState('')
    const [maxRange, setMaxRange] = useState(5)
    let history = useHistory()

    const { onNextIndex, planInfo, planDetails, index } = props
    useEffect(() => {
        AOS.init();
        AOS.refresh();
    }, []);

    const activeQuestion = planDetails[index]

    const clickOption = (id) => {
        setIdCheck(id)
    }

    const onClickRange = (value) => {

    }

    const classRange = (index) => {
        let tmpClass = ""
        if (!index) {
            tmpClass = "fir"
        }
        if (index == 9) {
            tmpClass = "last"
        }
        if (index <= maxRange) {
            tmpClass = tmpClass + " on"
        }
        if (index == maxRange) {
            tmpClass = tmpClass + " selected"
        }
        return tmpClass
    }

    return (
        <>
            <app-root ng-version="7.2.15">
                <div
                    id="header"
                    style={{
                        background: "transparent",
                        margin: "0 auto",
                        textAlign: "-webkit-center",
                    }}
                >
                    <img
                        alt="Huyndai"
                        className="d-none d-xl-block"
                        src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
                        style={{ width: 287 }}
                    />
                </div>
                <router-outlet />
                <app-survey-sale>
                    <div />
                    <div id="cBody">
                        <div className="survey">
                            <div
                                className="imgArea animate__animated animate__fadeInDownBig"
                                data-aos="fade-down"
                                data-aos-duration="600"
                                data-aos-offset="200"
                            >
                                <div className="bg">
                                    <div className="txtArea">
                                        <p className="num">1 / 13</p>
                                        <p className="tit">Giới thiệu</p>
                                    </div>
                                </div>
                                {/* <img
                    alt="Huyndai"
                    className="d-none d-xl-block"
                    src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
                    style={{ width: 287 }}
                /> */}
                                <img
                                    alt="Huyndai"
                                    src={process.env.PUBLIC_URL + "/images/hhh.gif"}
                                    style={{
                                        display: "inline",
                                        objecFit: "cover",
                                        width: "100%",
                                        height: "100%",
                                    }}
                                />
                            </div>
                            <div
                                className="surveyArea"
                                data-aos="fade-up"
                                data-aos-duration="600"
                                data-aos-offset="200"
                            >
                                <div className="ng-untouched ng-pristine ng-valid">
                                    <div className="qArea on animate__animated animate__fadeInUpBig">
                                        <div className="conDiv">
                                            <div className="table">
                                                <div className="table-cell 1">
                                                    <div className="txtArea on">
                                                        <div className="table">
                                                            <div className="table-cell">
                                                                <br />
                                                                <br />
                                                                <br />
                                                                <br />
                                                                <p className="q">
                                                                    <img
                                                                        src={
                                                                            process.env.PUBLIC_URL +
                                                                            "/images/q_tit1.png"
                                                                        }
                                                                        alt="Q"
                                                                    />
                                                                </p>
                                                                <p className="qTxt">
                                                                    {/**/}
                                                                    <span className="text-danger">*</span> Nếu có
                                  người thân, bạn bè sử dụng xe Hyundai, Quý
                                  khách có sẵn lòng giới thiệu đến Xưởng Dịch vụ
                                  của Đại lý?
                                </p>
                                                                <p className="pcTxt">
                                                                    1-&gt;6: Không giới thiệu; 7-&gt;8: Phân vân;
                                                                    9-&gt;10: Giới thiệu
                                </p>
                                                                {/**/}
                                                                <div className="scoreArea">
                                                                    <div className="btns">
                                                                        <span className="bad">1</span>
                                                                        {/* <a data-point={1} className="fir on ">
                                                                            <span>1</span>
                                                                        </a>
                                                                        <a data-point={2} className="on">
                                                                            <span>2</span>
                                                                        </a>
                                                                        <a data-point={3} className="on">
                                                                            <span>3</span>
                                                                        </a>
                                                                        <a data-point={4} className="on">
                                                                            <span>4</span>
                                                                        </a>
                                                                        <a data-point={5} className="on">
                                                                            <span>5</span>
                                                                        </a>
                                                                        <a data-point={6} className='on selected'>
                                                                            <span>6</span>
                                                                        </a>
                                                                        <a data-point={7} className>
                                                                            <span>7</span>
                                                                        </a>
                                                                        <a data-point={8} className>
                                                                            <span>8</span>
                                                                        </a>
                                                                        <a data-point={9} className>
                                                                            <span>9</span>
                                                                        </a>
                                                                        <a data-point={10} className="last ">
                                                                            <span>10</span>
                                                                        </a> */}

                                                                        {new Array(10).fill(1).map((i, idx) => {
                                                                            return <a style={{ cursor: 'pointer' }} onClick={() => setMaxRange(idx)} data-point={index + 1} className={classRange(idx)}>
                                                                                <span>{idx + 1}</span>
                                                                            </a>
                                                                        })}

                                                                        <span className="good">10</span>
                                                                    </div>
                                                                </div>
                                                                <div className="mobileScore">
                                                                    <select
                                                                        name="rating"
                                                                        className="ng-pristine ng-valid ng-touched"
                                                                    >
                                                                        <option value>Select</option>
                                                                        {/**/}
                                                                        <option value={10}>10 </option>
                                                                        <option value={9}>9 </option>
                                                                        <option value={8}>8 </option>
                                                                        <option value={7}>7 </option>
                                                                        <option value={6}>6 </option>
                                                                        <option value={5}>5 </option>
                                                                        <option value={4}>4 </option>
                                                                        <option value={3}>3 </option>
                                                                        <option value={2}>2 </option>
                                                                        <option value={1}>1 </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="txtArea" style={{ display: "block" }}>
                                                        <div className="table on">
                                                            <div className="table-cell">
                                                                <p className="q">
                                                                    <img
                                                                        src={
                                                                            process.env.PUBLIC_URL +
                                                                            "/images/q_tit2.png"
                                                                        }
                                                                        alt="Q+"
                                                                    />
                                                                </p>
                                                                <p className="qTxt">
                                                                    Các lí do khiến Quý khách không muốn giới
                                                                    thiệu Đại lý?
                                </p>
                                                                {/**/}
                                                                <div className="checkArea">
                                                                    {/**/}
                                                                    <p className="check">
                                                                        <input
                                                                            className="check-answer"
                                                                            name="check_answer"
                                                                            type="checkbox"
                                                                            id="chk1_670"
                                                                            defaultValue={670}
                                                                        />
                                                                        <label htmlFor="chk1_670">
                                                                            Cơ sở vật chất xưởng dịch vụ
                                    </label>
                                                                    </p>
                                                                    <p className="check">
                                                                        <input
                                                                            className="check-answer"
                                                                            name="check_answer"
                                                                            type="checkbox"
                                                                            id="chk1_671"
                                                                            defaultValue={671}
                                                                        />
                                                                        <label htmlFor="chk1_671">
                                                                            Thái độ nhân viên
                                    </label>
                                                                    </p>
                                                                    <p className="check">
                                                                        <input
                                                                            className="check-answer"
                                                                            name="check_answer"
                                                                            type="checkbox"
                                                                            id="chk1_672"
                                                                            defaultValue={672}
                                                                        />
                                                                        <label htmlFor="chk1_672">
                                                                            Chất lượng sửa chữa
                                    </label>
                                                                    </p>
                                                                    <p className="check">
                                                                        <input
                                                                            className="check-answer"
                                                                            name="check_answer"
                                                                            type="checkbox"
                                                                            id="chk1_705"
                                                                            defaultValue={705}
                                                                        />
                                                                        <label htmlFor="chk1_705">
                                                                            Thời gian phục vụ
                                    </label>
                                                                    </p>
                                                                    <p className="check">
                                                                        <input
                                                                            className="check-answer"
                                                                            name="check_answer"
                                                                            type="checkbox"
                                                                            id="chk1_706"
                                                                            defaultValue={706}
                                                                        />
                                                                        <label htmlFor="chk1_706">
                                                                            Giá trị dịch vụ nhận được
                                    </label>
                                                                    </p>
                                                                    <p className="check">
                                                                        <input
                                                                            className="check-answer"
                                                                            name="check_answer"
                                                                            type="checkbox"
                                                                            id="chk1_707"
                                                                            defaultValue={707}
                                                                        />
                                                                        <label htmlFor="chk1_707">
                                                                            Thủ tục giấy tờ
                                    </label>
                                                                    </p>

                                                                    {/**/}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="btnArea" style={{ display: "block" }}>
                                                        <a onClick={() => history.goBack()} className="grayBt prevBt" href="javascript:">
                                                            Quay lại
                            </a>
                                                        <a href="javascript:" className="blueBt nextBt ">
                                                            Tiếp Tục
                            </a>
                                                        {/**/}
                                                    </div>
                                                </div>
                                                {/**/}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </app-survey-sale>
                <div id="footer">
                    {" "}
          Copyright © 2018 Hyundai Motor Company. All rights reserved.
        </div>
            </app-root>
        </>

    );
}

export default TemplateSelect;
