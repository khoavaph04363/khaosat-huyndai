import React, { useState, useEffect } from "react";
import AOS from "aos";
import "aos/dist/aos.css";

function FormTemplate2(props) {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);

  return (
    <>
      <app-root ng-version="7.2.15">
        <div
          id="header"
          style={{
            background: "transparent",
            margin: "0 auto",
            textAlign: "-webkit-center",
          }}
        >
          <img
            alt="Huyndai"
            className="d-none d-xl-block"
            src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
            style={{ width: 287 }}
          />
        </div>
        <router-outlet />
        <app-survey-sale>
          <div />
          <div id="cBody">
            <div className="survey">
              <div
                className="imgArea animate__animated animate__fadeInDownBig"
                data-aos="fade-down"
                data-aos-duration="600"
                data-aos-offset="200"
              >
                <div className="bg">
                  <div className="txtArea">
                    <p className="num">1 / 13</p>
                    <p className="tit">Giới thiệu</p>
                  </div>
                </div>
                {/* <img
                    alt="Huyndai"
                    className="d-none d-xl-block"
                    src="https://hyundai.tcmotor.vn/images/logo-HTV-01.svg"
                    style={{ width: 287 }}
                /> */}
                <img
                  alt="Huyndai"
                  src={process.env.PUBLIC_URL + "/images/hhh.gif"}
                  style={{
                    display: "inline",
                    objecFit: "cover",
                    width: "100%",
                    height: "100%",
                  }}
                />
              </div>
              <div
                className="surveyArea"
                data-aos="fade-up"
                data-aos-duration="600"
                data-aos-offset="200"
              >
                <form noValidate className="ng-untouched ng-pristine ng-valid">
                  <div className="qArea on animate__animated animate__fadeInUpBig">
                    <div className="conDiv">
                      <div className="table">
                        <div className="table-cell 2">
                          <div className="txtArea on">
                            <div className="table">
                            <br />
                                <br />
                                <br />
                                <br />
                              <p className="q">
                                <img
                                  src={
                                    process.env.PUBLIC_URL +
                                    "/images/q_tit1.png"
                                  }
                                  alt="Q"
                                />
                              </p>
                              <p className="qTxt">
                                <span className="text-danger">*</span> Đại lý có
                                đề nghị cùng quý khách kiểm tra xe trước khi bắt
                                đầu công việc?
                              </p>
                              <p className="pcTxt" />
                              <p className="mobileTxt" />
                              <div className="checkArea type2">
                                {/**/}
                                <p className="radio">
                                  <input
                                    data-type="normal"
                                    name="answer"
                                    type="radio"
                                    id="r694"
                                    defaultValue={694}
                                  />
                                  <label htmlFor="r694">Có</label>
                                </p>
                                <p className="radio">
                                  <input
                                    data-type="normal"
                                    name="answer"
                                    type="radio"
                                    id="r695"
                                    defaultValue={695}
                                  />
                                  <label htmlFor="r695">Không</label>
                                </p>
                                <div>
                                  <p className="radio">
                                    <input
                                      className="etc"
                                      data-type="other"
                                      id="r_other"
                                      name="answer"
                                      type="radio"
                                    />
                                    <label htmlFor="r_other">Khác</label>
                                  </p>
                                  <p className="input">
                                    <input
                                      id="other"
                                      name="other"
                                      placeholder="Nhập text"
                                      type="text"
                                      disabled="disabled"
                                      className="ng-untouched ng-pristine ng-valid"
                                    />
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                          <div className="btnArea" style={{ display: "block" }}>
                            <a className="grayBt prevBt" href="javascript:">
                              Quay lại
                            </a>
                            <a href="javascript:" className="blueBt nextBt ">
                              Tiếp Tục
                            </a>
                            {/**/}
                          </div>
                        </div>

                        {/**/}
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </app-survey-sale>
        <div id="footer">
          {" "}
          Copyright © 2018 Hyundai Motor Company. All rights reserved.
        </div>
      </app-root>
    </>
  );
}

export default FormTemplate2;
