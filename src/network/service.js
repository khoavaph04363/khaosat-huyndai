import axios from "axios";

let dataService = {
  getInfo: (data) => {
    let url = "api/survey/get-info";
    return axios.post(url, data);
  },
  putAnswer: (data) => {
    let url = "/api/survey/v2/put-answer";
    return axios.post(url, data);
  },
  getCars: (data) => {
    let url = "/api/car/get-cars";
    return axios.post(url, data);
  },
  getAgency: (data) => {
    let url = "/api/agency/get-list-v2";
    return axios.post(url, data);
  },
  updateInfo: (data) => {
    let url = "/api/survey/update-info";
    return axios.post(url, data);
  },
};
export default dataService;
